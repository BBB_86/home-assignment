package hemuppgift;
import java.util.Scanner;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.SQLException;



public class Customer {
	Scanner input = new Scanner(System.in);	
	Connection myConn;
	Statement myStmt;
	ResultSet myRs;

	
		public Customer(String url, String user, String password) {
		
			
			try {
				myConn = DriverManager.getConnection(url,user,password);
			} catch (SQLException e) {
				e.printStackTrace();
			}		
		}
			public void customerMenu() {
			int select;
		
			do{
			
			System.out.println("(1) View available flights");
			System.out.println("(2) Book flight");
			System.out.println("(3) View booked flight");
			System.out.println("------------------------");
			System.out.println("(0) Return to main menu"); 
			System.out.println();
			select = input.nextInt();
			
			switch(select) {
			
			case 1:
				listFlights();//lista flights kan ta fr�n db direkt
				break;
			case 2:
				bookFlight();//boka flight
				break;
			case 3:
				showBooking();//se bokning
				break;
		
			case 0:
				Menu.select();//tillbaka till main menu
				break;
				
				
			}
			
			
				
			}while(!(select>=4));
		
	}
	
			public void listFlights() {
			Database viewFlights = new Database();
			viewFlights.showFlights();
		}
			
		
			public void bookFlight() { //bokar flight
				String cname;
				String famname;
				String cnumb;
				String cflight;
				String from;
				String to;
				Database book = new Database();
				System.out.println("Name ");	
				cname = input.next();
				
				System.out.println("Family name ");	
				famname = input.next();
				
				System.out.println("Phone number ");	
				cnumb = input.next();
				airportListing();
				System.out.println("Select departure airport (Airport name)");
				from = input.next();
				airportListing();
				System.out.println("Select destination (Airport name");
				to = input.next();
				System.out.println("Available flights for your selected route: // If no flights available, press (0) ");	
				searchListing(from, to);
				System.out.println("Select the flight number of the flight you want to book");
				cflight = input.next();
				System.out.println("Flight booked, have a nice day!");
				book.createBooking(cname, famname, cnumb, cflight);
			}
				
					
				public void showBooking() {
					String numb;
					Database show = new Database();
					System.out.println("Insert your phone number to view your booking");
					numb = input.next();
					System.out.println("Your reservation: ");
					show.custBooking(numb);
					
				}
					
				
		
				public void airportListing() {
				
				Database viewAirport = new Database();
				viewAirport.showAirports();
			}
		
			
				public void searchListing(String from, String to) {
				Database view = new Database();
				view.viewChoice(from, to);
			}
		
}



