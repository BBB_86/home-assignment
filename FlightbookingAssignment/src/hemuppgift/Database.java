package hemuppgift;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;



public class Database {

	
	private static String url = "jdbc:mysql://localhost:3306/";
	private static String user = "root";
	private static String password = "root";
	private static Connection myConn;
	private static Statement myStmt;
	public static String database = "flightbooking";
	
	
	public void createDatabase() {
		
		try{
			myConn = DriverManager.getConnection(url, user, password);
			Class.forName("com.mysql.jdbc.Driver");
			myStmt = myConn.createStatement();
	
			myStmt.executeUpdate("CREATE DATABASE IF NOT EXISTS " + database
			+ ";");
			
			myStmt.executeUpdate("CREATE TABLE IF NOT EXISTS " + database
			+ ".airline(airline_id INT NOT NULL AUTO_INCREMENT, airline_name VARCHAR(45) UNIQUE NOT NULL, PRIMARY KEY (airline_id));");
			
			myStmt.executeUpdate("CREATE TABLE IF NOT EXISTS "
					+ database
					+ ".airport(airport_id INT NOT NULL AUTO_INCREMENT, airport_name VARCHAR(45) UNIQUE NOT NULL, airport_location VARCHAR(45), PRIMARY KEY (airport_id));");
			
			myStmt.executeUpdate("CREATE TABLE IF NOT EXISTS "
					+ database
					+ ".flight(flight_id INT NOT NULL AUTO_INCREMENT, departure VARCHAR(45) NOT NULL, destination VARCHAR(45) NOT NULL, airline VARCHAR(45) NOT NULL, flight_number VARCHAR(45) NOT NULL, date VARCHAR(45) UNIQUE NOT NULL, PRIMARY KEY (flight_id));");
		
			myStmt.executeUpdate("CREATE TABLE IF NOT EXISTS "
					+ database
					+ ".booking(booking_id INT NOT NULL AUTO_INCREMENT,name VARCHAR(45) NOT NULL, family_name VARCHAR(45) NOT NULL,number VARCHAR(45) NOT NULL, flight VARCHAR(45) UNIQUE NOT NULL, PRIMARY KEY (booking_id));");
			
			
		
			
			
			//insertTest
			
			//airline
			
			myStmt.executeUpdate("INSERT IGNORE INTO "
					+ database
					+ ".airline(airline_name) VALUES ('Croatian');");
			myStmt.executeUpdate("INSERT IGNORE INTO "
					+ database
					+ ".airline(airline_name) VALUES ('Scandinavian');");
			myStmt.executeUpdate("INSERT IGNORE INTO "
					+ database
					+ ".airline(airline_name) VALUES ('Batman');");
			
			//airport
			
			myStmt.executeUpdate("INSERT IGNORE INTO "
					+ database
					+ ".airport(airport_name, airport_location) VALUES ('ZAG', 'Zagreb');");
			myStmt.executeUpdate("INSERT IGNORE INTO "
					+ database
					+ ".airport(airport_name, airport_location) VALUES ('BER', 'Berlin');");
			myStmt.executeUpdate("INSERT IGNORE INTO "
					+ database
					+ ".airport(airport_name, airport_location) VALUES ('CPH', 'Copenhagen');");
			myStmt.executeUpdate("INSERT IGNORE INTO "
					+ database
					+ ".airport(airport_name, airport_location) VALUES ('ARN', 'Arlanda');");
			//
			//flight
			myStmt.executeUpdate("INSERT IGNORE INTO "
					+ database
					+ ".flight(departure, destination, airline, flight_number, date) VALUES ('ZAG','BER','Croatian','AAA111','2015-02-02');");
			myStmt.executeUpdate("INSERT IGNORE INTO "
					+ database
					+ ".flight(departure, destination, airline, flight_number, date) VALUES ('BER','CPH','Batman','BBB222','2015-03-03');");
			myStmt.executeUpdate("INSERT IGNORE INTO "
					+ database
					+ ".flight(departure, destination, airline, flight_number, date) VALUES ('CPH','ARN','Scandinavian','CCC333','2015-04-04');");
			myStmt.executeUpdate("INSERT IGNORE INTO "
					+ database
					+ ".flight(departure, destination, airline, flight_number, date) VALUES ('ARN','BER','Batman','DDD444','2015-05-05');");
			
		
		
		
		}catch (ClassNotFoundException | SQLException exc){
				exc.printStackTrace();
		
		
		
		
		
		
		/////////////////////////////////////////////////////////////////////	
	}
	}	//Adminavdelning
			public void airportInsert(String name, String location) {
			try{
				
				
				myStmt = myConn.createStatement();
				
				myStmt.executeUpdate("INSERT IGNORE INTO flightbooking.airport(airport_name, airport_location) VALUES ('"+name+"', '"+location+"');");
			}
			catch (Exception exc){
				exc.printStackTrace();
		}
}
			public void airlineInsert(String airlineName) {
			try{
				
				myStmt = myConn.createStatement();
				myStmt.executeUpdate("INSERT IGNORE INTO flightbooking.airline(airline_name) VALUES ('"+airlineName+"');");
			
			}catch (Exception exc){
				exc.printStackTrace();
			}
		}
		
			public void flightInsert(String airl, String depart, String dest, String flNum, String date) {
			try{
				myStmt = myConn.createStatement();
				myStmt.executeUpdate("INSERT IGNORE INTO flightbooking.flight(airline, departure, destination, flight_number, date) "
						+ "VALUES ('"+airl+"','"+depart+"', '"+dest+"', '"+flNum+"', '"+date+"');");
				
				
			}catch (Exception exc){
				exc.printStackTrace();
			}
		}
			public void showAirports() {
			try{
				myStmt = myConn.createStatement();
				ResultSet myRs = myStmt.executeQuery("SELECT * from flightbooking.airport");
				
				while (myRs.next()){
					System.out.print("Airport: ");
					System.out.println(myRs.getString("airport_id")+"| Name "+myRs.getString("airport_name")+"| Location: "+myRs.getString("airport_location"));
					System.out.println("------------------------------------");
				}
			}catch (Exception exc){
				exc.printStackTrace();
			
			}
		}
			public void showAirlines() {
			try{
				myStmt = myConn.createStatement();
				ResultSet myRs = myStmt.executeQuery("SELECT * from flightbooking.airline");
				
				while (myRs.next()){
					System.out.print("Airline: ");
					System.out.println(myRs.getString("airline_id")+"| Name "+myRs.getString("airline_name"));
					System.out.println("---------------------------------------");
				}
			}catch (Exception exc){
				exc.printStackTrace();
			}
		}
			public void showFlights() {
			try{
				myStmt = myConn.createStatement();
				ResultSet myRs = myStmt.executeQuery("SELECT * from flightbooking.flight");
				
				while (myRs.next()){
					System.out.print("Flight: ");
					System.out.println(myRs.getString("flight_id")+"| Departure: "+myRs.getString("departure")+"| Destination: "+myRs.getString("destination")+"| Airline: "+myRs.getString("airline")+"| Flight number: "+myRs.getString("flight_number")+"| Date: "+myRs.getString("date"));
					System.out.println("------------------------------------------");
				}
			}catch (Exception exc){
				exc.printStackTrace();
			}
		}
			public void showBookings() {
			try{
				myStmt = myConn.createStatement();
				ResultSet myRs = myStmt.executeQuery("SELECT * from flightbooking.booking");
				
				while (myRs.next()){
					System.out.print("Booking: ");
					System.out.println(myRs.getString("booking_id")+"| Name: "+myRs.getString("name")+"| Family name: "+myRs.getString("family_name")+"| Phone number: "+myRs.getString("number")+"| Flight: "+myRs.getString("flight"));
					System.out.println("-------------------------------------------------------------------------------------------------");
				}
			}catch (Exception exc){
				exc.printStackTrace();
			}
		}
		
		////////////////////////////////////////////////////////////////////////////
		
		//Customeravdelning
		
			public void createBooking(String cname, String famname, String cnumb, String cflight) {
			try{
				myStmt = myConn.createStatement();
				myStmt.executeUpdate("INSERT IGNORE INTO flightbooking.booking(name, family_name, number, flight) VALUES ('"+cname+"','"+famname+"', '"+cnumb+"', '"+cflight+"');");
				
				
			}catch (Exception exc){
				exc.printStackTrace();
			}
		}
		
			public void viewChoice(String from, String to) {
			try{
				myStmt = myConn.createStatement();
				ResultSet myRs = myStmt.executeQuery("SELECT * from flightbooking.flight WHERE departure"
													+ " = '" + from + "' AND destination = '" + to
													+ "';");
				
				while (myRs.next()){
					System.out.print("Flight: ");
					System.out.println(myRs.getString("flight_id")+"| Departure: "+myRs.getString("departure")+"| Destination: "+myRs.getString("destination")+"| Airline: "+myRs.getString("airline")+"| Flight number: "+myRs.getString("flight_number")+"| Date: "+myRs.getString("date"));
					System.out.println("------------------------------------------");
				}
			}catch (Exception exc){
				exc.printStackTrace();
			}
		}
			public void custBooking(String numb) {
			try{
				myStmt = myConn.createStatement();
				ResultSet myRs = myStmt.executeQuery("SELECT * from flightbooking.booking WHERE number"
													+ " = '" + numb + "';");
				
				
				
			while (myRs.next()){
					System.out.println();
					System.out.println("Your reservation: ");
					System.out.print("Id: ");
					System.out.println(myRs.getString("booking_id")+"| Name: "+myRs.getString("name")+"| Family name: "+myRs.getString("family_name")+"| Number: "+myRs.getString("number")+"| Flight: "+myRs.getString("flight"));
					System.out.println("---------------------------------------------------------------------------------");
								}
		
	}catch (Exception exc){
		exc.printStackTrace();
	}
}
		}
		
		
		
		
		
		
		
		
		
		
			
			
		
		

