package hemuppgift;

public class Flight {
	
	public String airline;
	public String departure;
	public String destination;
	public String flightNum;
	public String date;
	
	public Flight(String airline, String departure, String destination, String flightNum, String date) {
		this.airline = airline;
		this.departure = departure;
		this.destination = destination;
		this.flightNum = flightNum;
		this.date = date;
	}
	public String getAirline() {
		return airline;
	}
	public void setAirline(String airline) {
		this.airline = airline;
	}
	public String getDeparture() {
		return departure;
	}
	public void setDeparture(String departure) {
		this.departure = departure;
	}	
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public String getFlightNum() {
		return flightNum;
	}
	public void setFlightNum(String flightNum) {
		this.flightNum =flightNum;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	
}
		
		


