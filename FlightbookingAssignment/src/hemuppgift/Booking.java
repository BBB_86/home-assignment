package hemuppgift;

public class Booking {
	private String name;
	private String famName;
	private String number;
	private String flight;
	
	public Booking(String name, String famName, String number, String flight) {
		this.name = name;
		this.famName = famName;
		this.number = number;
		this.flight = flight;
		
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getFamName() {
		return famName;
	}
	public void setFamName(String famName) {
		this.famName = famName;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getFlight() {
		return flight;
	}
	public void setFlight(String flight) {
		this.flight = flight;
	}

}
