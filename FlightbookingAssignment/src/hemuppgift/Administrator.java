package hemuppgift;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Scanner;
import java.sql.SQLException;

public class Administrator {// Admin skapar allt som admin ska skapa
	
	Scanner input = new Scanner(System.in);	
	Connection myConn;
	Statement myStmt;
	ResultSet myRs;
	
	
			public Administrator(String url, String user, String password) { 
	
			try {
				myConn = DriverManager.getConnection(url,user,password);
			} catch (SQLException e) {
				e.printStackTrace();
			}		
		}
	
	
	
			public void adminMenu() {
			int select;
		
			do{
			System.out.println("(1) Create airport");
			System.out.println("(2) Create airline");
			System.out.println("(3) Create flight");
			System.out.println("----------------------");
			System.out.println("(4) List all airports");
			System.out.println("(5) List all airlines");
			System.out.println("(6) List all flights");
			System.out.println("(7) List all bookings");
			System.out.println("------------------------");
			System.out.println("(0) Return to main menu");
			System.out.println();
			
			select = input.nextInt();
			
			switch(select) {
			case 1:
				airportCreate();//skapa flygplats 
				break;
			case 2:
				airlineCreate();//skapa flygbolag 
				break;
			case 3: 
				flightCreate();//skapa flight 
				break;
			case 4:
				airportListing();//visa flygplatser 
				break;
			case 5: 
				airlineListing();//visa flygbolag 
				break;
			case 6:
				flightListing();//visa flights 
				break;
			case 7:
				bookingListing();//visa alla bokningar 
				break;
			case 0:	
				Menu.select(); //tillbaka till main
				break;
				
			}
			}while(!(select>=8));
	}
	
			public void airportCreate() {
			String name;
			String location;
			Database airportIn = new Database();
			do{
				System.out.println("Insert name (3 letters)");
				name = input.next();
			if(!Regex.regexAirport(name)){
				System.out.println("Wrong name format, try again!");
			}
			}while (!Regex.regexAirport(name));
			System.out.println("Insert airport location");
			location = input.next();
			airportIn.airportInsert(name, location);
			
		}
		
	
			public void airlineCreate() {
			String airlineName;
			Database airlineIn = new Database();
			do{
				System.out.println("Insert airline name (6 characters)");
				airlineName = input.next();
				if(!Regex.regexAirline(airlineName)){
					System.out.println("Wrong name format, try again!");
			}
				}while (!Regex.regexAirline(airlineName));
				airlineIn.airlineInsert(airlineName);
		}
		
		

			public void flightCreate() { //skapar flight
			String airl;
			String depart;
			String dest;
			String flNum;
			String date;
			Database flightIn = new Database();
		
			airlineListing();
			System.out.println("Select airline (By name)");
			airl = input.next();
			airportListing();
			System.out.println("Select departure (By name)");
			depart = input.next();
			airportListing();
			System.out.println("Select destination (By name)");
			dest = input.next();
			System.out.println("Select Flightnumber (3 letters, 3 numbers");
			flNum = input.next();
			System.out.println("Select date");
			date = input.next();
			System.out.println("-----------------------------------");
			System.out.println("Flight created");
			System.out.println("------------------------------------");
			
			flightIn.flightInsert(airl, depart, dest, flNum, date);
		
		
		}
		
		
		
	
			public void airportListing() {
			Database viewAirport = new Database();
			viewAirport.showAirports();
		}
		
		
			public void airlineListing() {
			Database viewAirline = new Database();
			viewAirline.showAirlines();
		}
		
	
			public void flightListing() {
			Database viewFlights = new Database();
			viewFlights.showFlights();
		}
		
		
			public void bookingListing() {
			Database viewBookings = new Database();
			viewBookings.showBookings();
		}
		
		
}


